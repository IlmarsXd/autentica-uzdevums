package com.ilmars.webapp.database;

import com.ilmars.webapp.database.entities.Item;
import lombok.SneakyThrows;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ItemRepository extends BaseRepository<Item> {
    public ItemRepository(SqliteDatabase database) {
        super(database);
    }

    @SneakyThrows
    @Override
    public Item insert(Item item) {
        var sql = "INSERT INTO item_table(typeId, makerId, model, price) VALUES(?, ?, ?, ?) RETURNING id";
        return database.withConnection((connection) -> {
            var pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, item.typeId());
            pstmt.setInt(2, item.makerId());
            pstmt.setString(3, item.model());
            pstmt.setDouble(4, item.price());
            var rs = pstmt.executeQuery();
            rs.next();
            var res = rs.getInt("id");
            return new Item(res, item.typeId(), item.makerId(), item.model(), item.price());
        });

    }

    @SneakyThrows
    @Override
    public void delete(Item item) {
        var sql = "DELETE FROM item_table WHERE id = ?";
        database.withConnection((connection) -> {
            var prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, item.id());
            prepStatement.executeUpdate();
            return null;
        });
    }

    @SneakyThrows
    @Override
    public void update(Item item) {
        var sql = "UPDATE item_table SET typeId = ?, makerId = ?, model = ?, price = ? WHERE id = ?";
        database.withConnection((connection) -> {
            var prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, item.typeId());
            prepStatement.setInt(2, item.makerId());
            prepStatement.setString(3, item.model());
            prepStatement.setDouble(4, item.price());
            prepStatement.setInt(5, item.id());
            prepStatement.executeUpdate();
            return null;
        });
    }

    @SneakyThrows
    @Override
    public Item getOne(int id) {
        var sql = "SELECT * FROM item_table WHERE id = ?";
        return database.withConnection((connection) -> {
            var prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, id);
            var rs = prepStatement.executeQuery();
            rs.next();
            return mapToItem(rs);
        });
    }

    @SneakyThrows
    @Override
    public List<Item> getAll() {
        var sql = "SELECT * FROM item_table";
        return database.withConnection((connection) -> {
            try (PreparedStatement prepStatement = connection.prepareStatement(sql)) {
                var result = prepStatement.executeQuery();
                var items = new ArrayList<Item>();
                while (result.next()) {
                    items.add(mapToItem(result));
                }
                return items;
            }
        });
    }
    @SneakyThrows
    private Item mapToItem(ResultSet resultSet) {
        return new Item(
                resultSet.getInt("id"),
                resultSet.getInt("typeId"),
                resultSet.getInt("makerId"),
                resultSet.getString("model"),
                resultSet.getDouble("price")
        );
    }
}
