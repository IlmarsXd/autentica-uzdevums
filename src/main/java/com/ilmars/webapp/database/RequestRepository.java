package com.ilmars.webapp.database;

import com.ilmars.webapp.database.entities.Request;
import lombok.SneakyThrows;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class RequestRepository extends BaseRepository<Request> {
    public RequestRepository(SqliteDatabase database) {
        super(database);
    }

    @SneakyThrows
    @Override
    public Request insert(Request request) {
        String sql = "INSERT INTO request_table(reqName, reqSurname, reqEmail, reqPhone, itemId, explanation, date) VALUES(?, ?, ?, ?, ?, ?, ?) RETURNING id";
        return database.withConnection((connection) -> {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, request.name());
            pstmt.setString(2, request.surname());
            pstmt.setString(3, request.email());
            pstmt.setString(4, request.phone());
            pstmt.setInt(5, request.itemId());
            pstmt.setString(6, request.explanation());
            pstmt.setString(7, request.date());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            int res = rs.getInt("id");
            return new Request(res, request.name(), request.surname(), request.email(), request.phone(), request.itemId(), request.explanation(), request.date());
        });
    }

    @SneakyThrows
    @Override
    public void delete(Request request) {
        String sql = "DELETE FROM request_table WHERE id = ?";
        database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, request.id());
            prepStatement.executeUpdate();
            return null;
        });
    }

    @SneakyThrows
    @Override
    public void update(Request item) {
        String sql = "UPDATE request_table SET reqName = ?, reqSurname = ?, reqEmail = ?, reqPhone = ?, itemId = ?, explanation = ?, date = ? WHERE id = ?";
        database.withConnection((connection) -> {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, item.name());
            pstmt.setString(2, item.surname());
            pstmt.setString(3, item.email());
            pstmt.setString(4, item.phone());
            pstmt.setInt(5, item.itemId());
            pstmt.setString(6, item.explanation());
            pstmt.setString(7, item.date());
            pstmt.setInt(8, item.id());
            pstmt.executeUpdate();
            return null;
        });
    }

    @SneakyThrows
    @Override
    public Request getOne(int id) {
        String sql = "SELECT * FROM request_table WHERE id = ?";
        return database.withConnection((connection) -> {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                return mapToRequest(resultSet);
            }
            return null;
        });
    }

    @SneakyThrows
    @Override
    public List<Request> getAll() {
        String sql = "SELECT * FROM request_table";
        return database.withConnection((connection) -> {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            ResultSet resultSet = pstmt.executeQuery();
            List<Request> requests = new ArrayList<>();
            while (resultSet.next()) {
                requests.add(mapToRequest(resultSet));
            }
            return requests;
        });
    }
    @SneakyThrows
    private Request mapToRequest(ResultSet resultSet) {
        return new Request(
                resultSet.getInt("id"),
                resultSet.getString("reqName"),
                resultSet.getString("reqSurname"),
                resultSet.getString("reqEmail"),
                resultSet.getString("reqPhone"),
                resultSet.getInt("itemId"),
                resultSet.getString("explanation"),
                resultSet.getString("date")
        );
    }
    public Request deleteRequest(int id) {
        Request request = getOne(id);
        delete(request);
        return request;
    }
}
