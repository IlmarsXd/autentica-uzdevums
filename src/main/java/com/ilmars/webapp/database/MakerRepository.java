package com.ilmars.webapp.database;

import com.ilmars.webapp.database.entities.Maker;
import lombok.SneakyThrows;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MakerRepository extends BaseRepository<Maker> {
    public MakerRepository(SqliteDatabase database) {
        super(database);
    }

    @Override
    public Maker insert(Maker item) {
        String sql = "INSERT INTO maker_table(maker) VALUES(?) RETURNING id";
        return database.withConnection((connection) -> {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, item.maker());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            int res = rs.getInt("id");
            return new Maker(res, item.maker());
        });
    }

    @Override
    public void delete(Maker item) {
        String sql = "DELETE FROM maker_table WHERE id = ?";
        database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, item.id());
            prepStatement.executeUpdate();
            return null;
        });
    }

    @Override
    public void update(Maker item) {
        String sql = "UPDATE maker_table SET maker = ? WHERE id = ?";
        database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            prepStatement.setString(1, item.maker());
            prepStatement.setInt(2, item.id());
            prepStatement.executeUpdate();
            return null;
        });
    }

    @Override
    public Maker getOne(int id) {
        String sql = "SELECT * FROM maker_table WHERE id = ?";
        return database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, id);
            ResultSet rs = prepStatement.executeQuery();
            rs.next();
            return mapToMaker(rs);
        });
    }

    @Override
    public List<Maker> getAll() {
        String sql = "SELECT * FROM maker_table";
        return database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            ResultSet rs = prepStatement.executeQuery();
            List<Maker> makers = new ArrayList<>();
            while (rs.next()) {
                makers.add(mapToMaker(rs));
            }
            return makers;
        });
    }
    @SneakyThrows
    private Maker mapToMaker(ResultSet rs) {
        return new Maker(rs.getInt("id"), rs.getString("maker"));
    }
}
