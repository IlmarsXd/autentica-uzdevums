package com.ilmars.webapp.database;

import com.ilmars.webapp.database.entities.ItemType;
import lombok.SneakyThrows;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ItemTypeRepository extends BaseRepository<ItemType> {
    public ItemTypeRepository(SqliteDatabase database) {
        super(database);
    }

    @SneakyThrows
    @Override
    public ItemType insert(ItemType itemType) {
        String sql = "INSERT INTO item_type_table(type) VALUES(?) RETURNING id";
        return database.withConnection((connection) -> {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, itemType.type());
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            int res = rs.getInt("id");
            return new ItemType(res, itemType.type());
        });
    }

    @SneakyThrows
    @Override
    public void delete(ItemType itemType) {
        String sql = "DELETE FROM item_type_table WHERE id = ?";
        database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, itemType.id());
            prepStatement.executeUpdate();
            return null;
        });
    }

    @SneakyThrows
    @Override
    public void update(ItemType itemType) {
        String sql = "UPDATE item_type_table SET type = ? WHERE id = ?";
        database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            prepStatement.setString(1, itemType.type());
            prepStatement.setInt(2, itemType.id());
            prepStatement.executeUpdate();
            return null;
        });
    }

    @SneakyThrows
    @Override
    public ItemType getOne(int id) {
        String sql = "SELECT * FROM item_type_table WHERE id = ?";
        return database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            prepStatement.setInt(1, id);
            ResultSet rs = prepStatement.executeQuery();
            rs.next();
            return mapToItemType(rs);
        });
    }

    @SneakyThrows
    @Override
    public List<ItemType> getAll() {
        String sql = "SELECT * FROM item_type_table";
        return database.withConnection((connection) -> {
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            ResultSet rs = prepStatement.executeQuery();
            List<ItemType> itemTypes = new ArrayList<>();
            while (rs.next()) {
                itemTypes.add(mapToItemType(rs));
            }
            return itemTypes;
        });
    }
    @SneakyThrows
    private ItemType mapToItemType(ResultSet rs) {
        return new ItemType(rs.getInt("id"), rs.getString("type"));
    }
}
