package com.ilmars.webapp.database;

import lombok.SneakyThrows;
import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SqliteDatabase {
    String connUrl, name;
    SQLiteConfig config;

    public SqliteDatabase(String name) {
        this.name = name;
        this.connUrl = "jdbc:sqlite:" + name;
        this.config = new SQLiteConfig();
        this.config.enforceForeignKeys(true);
        getConnection();
    }

    @SneakyThrows
    private void getConnection() {
        Class.forName("org.sqlite.JDBC");
        try (Connection conn = connect()) {
            if (conn == null) return;
            System.out.println("Veiksmīgi pieslēdzās datubāzei " + name);
        }
    }

    public void createNewTable(String name, TableEntry... entries) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("CREATE TABLE IF NOT EXISTS ").append(name).append(" (\n");
        for (TableEntry entry : entries) {
            sqlBuilder.append(entry.name()).append(" ").append(entry.type());
            if (entry.isPrimary()) {
                sqlBuilder.append(" PRIMARY KEY");
            }

            if (entry.isAutoIncerement()) {
                sqlBuilder.append(" AUTOINCREMENT");
            }

            if (entry.foreignKey() != null) {
                sqlBuilder.append(entry.foreignKey());
            }

            if (entry != entries[entries.length - 1]) {
                sqlBuilder.append(",\n");
            }
        }
        sqlBuilder.append("\n);");
        executeQuery(sqlBuilder.toString());
    }

    public void executeQuery(String sql) {
        withConnection((connection) -> {
            try (Statement stmt = connection.createStatement()) {
                stmt.execute(sql);
            }
            return null;
        });
    }

    @SneakyThrows
    public <T> T withConnection(ConnectionCallback<T> callback) {
        try (Connection conn = connect()) {
            if (conn == null) {
                throw new NullPointerException("Netika izveidots savienojums ar datubāzi " + name);
            }
            return callback.useConnection(conn);
        }
    }

    public Connection connect() throws SQLException {
        return DriverManager.getConnection(connUrl, config.toProperties());
    }
}
