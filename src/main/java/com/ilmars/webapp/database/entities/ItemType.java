package com.ilmars.webapp.database.entities;

public record ItemType(Integer id, String type) {
    public ItemType(String type) {
        this(null, type);
    }
}
