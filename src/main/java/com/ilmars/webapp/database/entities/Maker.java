package com.ilmars.webapp.database.entities;

public record Maker(Integer id, String maker) {
    public Maker(String maker) {
        this(null, maker);
    }
}
