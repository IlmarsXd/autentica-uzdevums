package com.ilmars.webapp.database.entities;

public record Item(Integer id, int typeId, int makerId, String model, double price) {
    public Item(int typeId, int makerId, String model, double price) {
        this(null, typeId, makerId, model, price);
    }
}
