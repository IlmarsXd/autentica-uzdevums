package com.ilmars.webapp.database.entities;

public record Request(Integer id, String name, String surname, String email, String phone, int itemId, String explanation, String date) {
    public Request(String name, String surname, String email, String phone, int itemId, String explanation, String date) {
        this(null, name, surname, email, phone, itemId, explanation, date);
    }
}
