package com.ilmars.webapp.database;

import java.util.List;

public abstract class BaseRepository<T> {
    protected final SqliteDatabase database;

    public BaseRepository(SqliteDatabase database) {
        this.database = database;
    }

    // Ieraksta ievietošana datu bāzē
    public abstract T insert(T item);

    // Ieraksta izdzēšana no datu bāzes
    public abstract void delete(T item);

    // Ieraksta atjaunināšana datu bāzē
    public abstract void update(T item);

    // Ieraksta iegūšana no datu bāzes
    public abstract T getOne(int id);

    // Visu ierakstu iegūšana no datu bāzes
    public abstract List<T> getAll();
}
