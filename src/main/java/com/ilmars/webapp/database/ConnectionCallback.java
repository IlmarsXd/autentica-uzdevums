package com.ilmars.webapp.database;

import java.sql.Connection;

@FunctionalInterface
public interface ConnectionCallback<T> {
    T useConnection(Connection connection) throws Exception;
}
