package com.ilmars.webapp.database;

public record TableEntry(String name, String type, boolean isPrimary, boolean isAutoIncerement, String foreignKey) {
    public TableEntry(String foreignKey) {
        this("", "", false, false, foreignKey);
    }
    public TableEntry(String name, String type, boolean isPrimary, boolean isAutoIncerement) {
        this(name, type, isPrimary, isAutoIncerement, null);
    }

    public TableEntry(String name, String type, boolean isPrimary) {
        this(name, type, isPrimary, false, null);
    }

    public TableEntry(String name, String type) {
        this(name, type, false, false, null);
    }
}
