package com.ilmars.webapp.handlebars;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;

import java.util.Map;

public class ItemsLookupHelper implements Helper<Object> {
    @Override
    public Object apply(Object context, Options options) {
        var element = options.param(0);
        if (context instanceof Map<?, ?> map) {
            for (var entry : map.entrySet()) {
                if (entry.getKey().equals(element)) {
                    return entry.getValue();
                }
            }
        }
        return null;
    }
}
