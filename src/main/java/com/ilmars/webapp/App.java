package com.ilmars.webapp;

import com.github.jknack.handlebars.Handlebars;
import com.ilmars.webapp.database.*;
import com.ilmars.webapp.database.entities.Item;
import com.ilmars.webapp.database.entities.ItemType;
import com.ilmars.webapp.database.entities.Maker;
import com.ilmars.webapp.database.entities.Request;
import com.ilmars.webapp.handlebars.ItemsLookupHelper;
import io.jooby.Context;
import io.jooby.Jooby;
import io.jooby.ModelAndView;
import io.jooby.handlebars.HandlebarsModule;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.stream.Collectors;

public class App extends Jooby {
    // izveido objektus darbībām ar tabulām
    private ItemTypeRepository itemTypeRepository;
    private MakerRepository makerRepository;
    private ItemRepository itemRepository;
    private RequestRepository requestRepository;

    {
        // izveido jaunu Templating Engine darbībai ar html failiem.
        Handlebars handlebars = new HandlebarsModule.Builder().setTemplatesPath("views").build(getEnvironment());
        handlebars.registerHelper("itemsLookup", new ItemsLookupHelper());
        install(new HandlebarsModule(handlebars));

        // inicializācija un novirzes
        onStarted(this::initDatabase);
        get("/", this::getMainPage);
        get("/error", this::getErrorPage);
        get("/new-request", this::getAddRequestPage);
        get("/requests", this::getRequestsPage);

        // pieprasījumu apstrāde
        post("/new-request", ctx -> {
            // Regular Expression priekš latviešu telefona numura
            var phoneRegex = "^\\+?\\d{8,12}$";

            // tiek iegūti dati no aizpildītās formas un tiek izveidots jauns pieprasījums.
            var reqName = ctx.form("reqName").value();
            var reqSurname = ctx.form("reqSurname").value();
            var reqEmail = ctx.form("reqEmail").value();
            var reqPhone = ctx.form("reqPhone").value();
            var itemId = Integer.parseInt(ctx.form("item").value().split(",")[0]);
            var explanation = ctx.form("explanation").value();

            if (reqName.length() > 40 || reqSurname.length() > 40 || reqEmail.length() > 40 || !reqPhone.matches(phoneRegex) || explanation.length() > 400) {
                ctx.sendRedirect("/error");
                return "";
            }

            requestRepository.insert(new Request(reqName, reqSurname, reqEmail, "+371 " + reqPhone, itemId, explanation, now()));
            // novirza uz galveno lapu
            ctx.sendRedirect("/");
            return "";
        });

        post("/delete", ctx -> {
            var id = Integer.parseInt(ctx.form("id").value());
            requestRepository.deleteRequest(id);
            ctx.sendRedirect("/requests");
            return "";
        });
    }

    public static void main(String[] args) {
        if (args.length == 0 || !args[0].equalsIgnoreCase("run")) System.exit(0);
        runApp(args, App::new);
    }

    private void initDatabase() {
        // izveidojam datu bāzi un tabulas.
        var database = new SqliteDatabase("system.db");

        database.createNewTable("item_type_table",
                new TableEntry("id", "INTEGER", true, true),
                new TableEntry("type", "VARCHAR(40)")
        );

        database.createNewTable("maker_table",
                new TableEntry("id", "INTEGER", true, true),
                new TableEntry("maker", "VARCHAR(40)")
        );

        database.createNewTable("item_table",
                new TableEntry("id", "INTEGER", true, true),
                new TableEntry("typeId", "INTEGER"),
                new TableEntry("makerId", "INTEGER"),
                new TableEntry("model", "VARCHAR(40)"),
                new TableEntry("price", "DOUBLE"),
                new TableEntry("FOREIGN KEY (typeId) REFERENCES item_type_table(id)"),
                new TableEntry("FOREIGN KEY (makerId) REFERENCES maker_table(id)")
        );

        database.createNewTable("request_table",
                new TableEntry("id", "INTEGER", true, true),
                new TableEntry("reqName", "VARCHAR(40)"),
                new TableEntry("reqSurname", "VARCHAR(40)"),
                new TableEntry("reqEmail", "VARCHAR(40)"),
                new TableEntry("reqPhone", "VARCHAR(13)"),
                new TableEntry("itemId", "INTEGER"),
                new TableEntry("explanation", "TEXT"),
                new TableEntry("date", "VARCHAR(20)"),
                new TableEntry("FOREIGN KEY (itemId) REFERENCES item_table(id)")
        );

        // izveidojam tabulu lauku objektus, kas pieņem attiecīgas vērtības pievienošanai datu bāze.
        itemTypeRepository = new ItemTypeRepository(database);
        makerRepository = new MakerRepository(database);
        itemRepository = new ItemRepository(database);
        requestRepository = new RequestRepository(database);

        // ja šī tabula ir tukša, tas nozīmē ka visa datu bāze ir tukša - pievienojam ierakstus testēšanai.
        if (itemTypeRepository.getAll().size() != 0) return;

        // pievienojam tehnikas veidus testēšanai.
        itemTypeRepository.insert(new ItemType("Laptop"));
        itemTypeRepository.insert(new ItemType("Phone"));
        itemTypeRepository.insert(new ItemType("Tablet"));
        itemTypeRepository.insert(new ItemType("TV"));
        itemTypeRepository.insert(new ItemType("Monitor"));
        itemTypeRepository.insert(new ItemType("Keyboard"));
        itemTypeRepository.insert(new ItemType("Mouse"));
        itemTypeRepository.insert(new ItemType("Headphones"));
        itemTypeRepository.insert(new ItemType("Speaker"));

        // pievienojam ražotājus testēšanai.
        makerRepository.insert(new Maker("Apple"));
        makerRepository.insert(new Maker("Dell"));
        makerRepository.insert(new Maker("Lenovo"));
        makerRepository.insert(new Maker("Samsung"));
        makerRepository.insert(new Maker("Google"));
        makerRepository.insert(new Maker("LG"));
        makerRepository.insert(new Maker("Sony"));
        makerRepository.insert(new Maker("Acer"));
        makerRepository.insert(new Maker("Logitech"));
        makerRepository.insert(new Maker("Microsoft"));

        // pievienojam pievienojam tehniku testēšanai.
        itemRepository.insert(new Item(1, 1, "MacBook Pro", 2000.0));
        itemRepository.insert(new Item(1, 2, "XPS 13", 1500.0));
        itemRepository.insert(new Item(1, 3, "ThinkPad X1 Carbon", 1500.0));

        itemRepository.insert(new Item(2, 1, "iPhone 12 Pro", 1000.0));
        itemRepository.insert(new Item(2, 4, "Galaxy S21 Ultra", 1000.0));
        itemRepository.insert(new Item(2, 5, "Pixel 5", 700.0));

        itemRepository.insert(new Item(3, 1, "iPad Pro", 1000.0));
        itemRepository.insert(new Item(3, 4, "Galaxy Tab S7", 1000.0));
        itemRepository.insert(new Item(3, 5, "Pixel Slate", 700.0));

        itemRepository.insert(new Item(4, 4, "QLED 8K", 5000.0));
        itemRepository.insert(new Item(4, 6, "OLED 4K", 3000.0));
        itemRepository.insert(new Item(4, 7, "XBR 4K", 3000.0));

        itemRepository.insert(new Item(5, 4, "C27F591", 300.0));
        itemRepository.insert(new Item(5, 6, "27GL83A", 300.0));
        itemRepository.insert(new Item(5, 7, "ED273", 300.0));

        itemRepository.insert(new Item(6, 1, "Magic Keyboard", 200.0));
        itemRepository.insert(new Item(6, 9, "K780", 100.0));
        itemRepository.insert(new Item(6, 10, "Surface Keyboard", 100.0));

        itemRepository.insert(new Item(7, 1, "Magic Mouse 2", 100.0));
        itemRepository.insert(new Item(7, 9, "MX Master 3", 100.0));
        itemRepository.insert(new Item(7, 10, "Surface Mouse", 100.0));

        itemRepository.insert(new Item(8, 1, "AirPods Pro", 200.0));
        itemRepository.insert(new Item(8, 4, "Galaxy Buds Pro", 200.0));
        itemRepository.insert(new Item(8, 7, "WF-1000XM3", 200.0));

        itemRepository.insert(new Item(9, 1, "HomePod", 300.0));
        itemRepository.insert(new Item(9, 5, "Nest Audio", 100.0));
        itemRepository.insert(new Item(9, 7, "HT-S350", 100.0));

        // pievienojam pieprasījumus testēšanai.
        requestRepository.insert(new Request("John", "Doe", "john.doe@gmail.com", "+371 12345678", 1, "I want to buy this laptop.", now()));
        requestRepository.insert(new Request("Jane", "Doe", "jane.doe@gmail.com", "+371 87654321", 2, "I want to buy this phone.", now()));
    }

    private ModelAndView getAddRequestPage(Context ctx) {
        return new ModelAndView("addRequest.html")
                .put("itemTypes", itemTypeRepository.getAll())
                .put("makers", makerRepository.getAll())
                .put("items", itemRepository.getAll());
    }

    private Object getRequestsPage(Context ctx) {
        var requests = requestRepository.getAll();
        // tiek formatēts tehnikas nosaukums pieprasījuma tabulā.
        var formattedItems = itemRepository.getAll().stream().collect(Collectors.toMap(Item::id, (item) -> itemTypeRepository.getOne(item.typeId()).type() + ", " + makerRepository.getOne(item.makerId()).maker() + " " + item.model()));
        return new ModelAndView("requests.html")
                .put("requests", requests)
                .put("items", formattedItems);
    }

    private Object getMainPage(Context ctx) {
        return new ModelAndView("index.html");
    }

    private Object getErrorPage(Context context) {
        return new ModelAndView("error.html");
    }
    public static String now() {
        // formatēšana laika attēlošanai pieprasījuma tabulā.
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());
    }
}
